<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Facebook\Facebook;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\FacebookFeed;

class FacebookFeedController extends Controller {

    const ACCESS_TOKEN = '551011271929076|gyouiPhjBlyKHpiRkffV_9AW5LI';
    const APP_ID = '551011271929076';
    const APP_SECRET = 'a79cf3f69e01f854fea81f5a08e08e6e';
    const DEFAULT_GRAPH_VERSION = 'v2.12';
    private $facebook;
    private $doctrineManager;
    
    /**
     * @Route("/")
     */
    public function index(Request $request) {
               
        $this->doctrineManager = $this->getDoctrine()->getManager();
        
        $this->facebookConnect();        
        
        $page = $this->getPage('dentsuaegisglobal', 100);
        
        $this->savePage($page);
   
        $form = $this->sortingForm($request);
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $order = $form->getData()['sort'];
        } else {
            $order = 'created';
        }   
        
        $query = $this->doctrineManager->createQuery("
            SELECT p FROM App:FacebookFeed p 
            ORDER BY p.$order DESC
        ");
        

        return $this->render(
            'facebook_feeds.html.twig',
            array(
                'facebookFeeds' => $query->getResult(),
                'form' => $form->createView()
            )
        );
    }
    
    /*
     * Pobiera managera Facebooka
     */
    private function facebookConnect() {
        $this->facebook = new Facebook([
            'app_id' => self::APP_ID,
            'app_secret' => self::APP_SECRET,
            'default_graph_version' => self::DEFAULT_GRAPH_VERSION
        ]);
    }
    
    /*
     * Pobiera feedy ze wskazanej strony
     * @param int $page_id
     * @param int $limit
     * @return \Facebook\GraphNodes\GraphEdge
     */
    private function getPage(string $page_id, int $limit = 100) {        
        return $this->getFacebookResponse("/$page_id/feed?limit=$limit")->getGraphEdge();
    }

    /*
     * Pobiera komentarze z noda
     * @param string $nodeId
     * @return Array
     */
    private function getNodeComments(string $nodeId) {
        $commentsEdge = $this->getFacebookResponse("/$nodeId/comments")->getGraphEdge();
        $comments = [];
        foreach($commentsEdge AS $comment) {                
            $comments[] = $comment->getField('message');
        }
        return $comments;
    }
    
    /*
     * Pobiera polubienia
     * @param string $object_id
     * @return \Facebook\GraphNodes\GraphEdge
     */    
    private function getLikes(string $object_id) {
        return $this->getFacebookResponse("/$object_id/likes")->getGraphEdge();
    }    

    /*
     * Zapisuje feedy do bazy
     * @param \Facebook\GraphNodes\GraphEdge $page
     */
    private function savePage($page) {        
        $query = $this->doctrineManager->createQuery("SELECT count(p) AS feedCount FROM App:FacebookFeed p");
      
        if($query->getResult()[0]['feedCount'] >= 100) {
            return;
        }

        foreach ($page as $node) {
            $description = $node->getField('message');           
            $comments = $this->getNodeComments($node->getField('id'));
            $description .=  implode("breakpage", $comments);
             
            $feed = new FacebookFeed();
            $feed->setAuthor("DentsuAegisNetwork");            
            $feed->setDescription(preg_replace('/[^A-Za-z0-9\-()<>= "\/]/', '', $description));
            $feed->setCreated($node->getField('created_time'));
            $feed->setCommentsCount(count($comments));
            $feed->setLikesCount(0);
            
            try {
                $this->doctrineManager->persist($feed);
            } catch (Exception $ex) {

            }
            
            $this->doctrineManager->flush(); 
        }
    }
    
    
    /*
     * Tworzy formularz sortowania
     * @param Request $request
     * @return FormBuilderInterface
     */
    private function sortingForm(Request $request) {
        $form = $this->createFormBuilder()
            ->add('sort', ChoiceType::class, array(
                'choices'   => array( 
                    'Autor' =>'author',  
                    'Data utworzenia' => 'created' , 
                    'Liczba komentarzy' => 'commentsCount', 
                    'Liczba polubień' => 'likesCount'
                    ),
                'required'  => false,
            ))
            ->add('send', SubmitType::class, array('label' => 'Wybierz'))
            ->getForm();
        
        return $form;
    }

        /*
     * Tworzy formularz sortowania
     * @param Request $request
     * @return FormBuilderInterface
     */
    private function sortingFormClass(Request $request) {
        $form = $this->createFormBuilder()
            ->add('sort', EntityType::class, array(
                'class' => FacebookFeed::class
            ))
            ->add('send', SubmitType::class, array('label' => 'Wybierz'))
            ->getForm();
        
        return $form;
    }
   
    /*
     * Wysyła zapytania do Facebooka
     * @param string $query
     * @return FacebookResponse
     */
    private function getFacebookResponse(string $query) {
        try {
            $response = $this->facebook->get($query, self::ACCESS_TOKEN);
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        return $response;
    }

}

?>