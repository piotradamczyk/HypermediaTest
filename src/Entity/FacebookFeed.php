<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FacebookFeedRepository")
 */
class FacebookFeed {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $author;

    /**
     * @ORM\Column(type="datetime") 
     */
    protected $created;

    /**
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @ORM\Column(type="integer")
     */
    protected $likesCount;

    /**
     * @ORM\Column(type="integer")
     */
    protected $commentsCount;

    public function getId() {
        return $this->id;
    }

    public function getAuthor() {
        return $this->author;
    }

    public function getCreated() {
        return $this->created;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getLikesCount() {
        return $this->likesCount;
    }
    
    public function getCommentsCount() {
        return $this->commentsCount;
    }
    
    public function setId(int $id) {
        $this->id = $id;
    }

    public function setAuthor(string $author) {
        $this->author = $author;
    }

    public function setCreated(\DateTime $created) {
        $this->created = $created;
    }

    public function setDescription(string $description) {
        $this->description = $description;
    }

    public function setLikesCount(int $likesCount) {
        $this->likesCount = $likesCount;
    }
    
    public function setCommentsCount(int $commentsCount) {
        $this->commentsCount = $commentsCount;
    }

}
